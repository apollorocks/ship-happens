﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifetimeController : MonoBehaviour {
	public float lifeTime = 1.0f;

	float startTime = 0f;

	// Use this for initialization
	void Start () {
		startTime = Time.fixedTime;	
	}
	
	// Update is called once per frame
	void Update () {
	}

	void LateUpdate() {
		if (Time.fixedTime - startTime > lifeTime) {
			GameObject.Destroy (gameObject);
		}
	}
}
