﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {
	public float forceFactor = 3000f;
	public float maxVelocity = 30f;
	public GameObject ammoPrefab;
	public float firingForce = 10f;
	public float firingInterval = 0.25f;

	Rigidbody shipPhysics;
	GameObject firingPoint;
	float lastFire = 0f;

	// Use this for initialization
	void Start () {
		shipPhysics = gameObject.GetComponent<Rigidbody> ();
		firingPoint = gameObject.transform.Find ("FiringPoint").gameObject;

		lastFire = Time.fixedTime;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void LateUpdate() {
		ApplyHorizontalInput ();

		HandleFireInput ();
	}

	void ApplyHorizontalInput () {
		float xInput = Input.GetAxis ("Horizontal");
		if (xInput == 0f) {
			shipPhysics.drag = 5.0f;
			return;
		}

		shipPhysics.drag = 0f;


		var force = xInput * forceFactor * Time.deltaTime;
		force *= (maxVelocity - Mathf.Abs (shipPhysics.velocity.x)) / maxVelocity;

		shipPhysics.AddForce(new Vector3(force, 0f, 0f));
	}

	void HandleFireInput () {
		if (Time.fixedTime - lastFire < firingInterval) {
			return;
		}
	
		bool hasFireButton = Input.GetButton ("Fire1");
		if (!hasFireButton || ammoPrefab == null) {
			return;
		}

		lastFire = Time.fixedTime;

		var ammoObject = GameObject.Instantiate (ammoPrefab);
		ammoObject.transform.position = firingPoint.transform.position;

		var ammoPhysics = ammoObject.GetComponent<Rigidbody> ();
		var ammoForce = new Vector3 (0f, 0f, firingForce);
		ammoPhysics.AddForce (ammoForce);
	}
}
