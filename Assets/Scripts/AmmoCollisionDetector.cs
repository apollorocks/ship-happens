﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoCollisionDetector : MonoBehaviour {
	ParticleSystem explosion;
	AudioSource explosionSound;
	Text scoreText;
	Text winningText;
	bool isDying = false;
	// Use this for initialization
	void Start () {
		explosion = gameObject.transform.Find ("Explosion").gameObject.GetComponent<ParticleSystem> ();
		explosionSound = gameObject.GetComponent<AudioSource> ();
		scoreText = GameObject.Find ("ScoreText").gameObject.GetComponent<Text>();
		winningText = GameObject.Find ("WinningText").gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collission) {
		if (collission.gameObject.tag == "Ammo" && !isDying) {
			GameObject.Destroy (collission.gameObject);
			GameObject.Destroy (gameObject, 0.5f);
			explosion.Play ();
			explosionSound.Play ();

			ShipHappensGame.currentScore += 1530;

			scoreText.text = "Score: " + ShipHappensGame.currentScore;

			isDying = true;

			ShipHappensGame.totalEnemyShipDestroyed++;

			if (ShipHappensGame.totalEnemyShip - ShipHappensGame.totalEnemyShipDestroyed <= 0) {
				winningText.text = "Mission Completed!!!";
				winningText.gameObject.SetActive (true);
			}
		}
	}
}
